package org.pshdl.commandline;

import java.io.File;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.eclipse.lsp4j.CodeActionParams;
import org.eclipse.lsp4j.CodeLens;
import org.eclipse.lsp4j.CodeLensParams;
import org.eclipse.lsp4j.Command;
import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionItemKind;
import org.eclipse.lsp4j.CompletionList;
import org.eclipse.lsp4j.DidChangeTextDocumentParams;
import org.eclipse.lsp4j.DidCloseTextDocumentParams;
import org.eclipse.lsp4j.DidOpenTextDocumentParams;
import org.eclipse.lsp4j.DidSaveTextDocumentParams;
import org.eclipse.lsp4j.DocumentFormattingParams;
import org.eclipse.lsp4j.DocumentHighlight;
import org.eclipse.lsp4j.DocumentOnTypeFormattingParams;
import org.eclipse.lsp4j.DocumentRangeFormattingParams;
import org.eclipse.lsp4j.DocumentSymbolParams;
import org.eclipse.lsp4j.Hover;
import org.eclipse.lsp4j.Location;
import org.eclipse.lsp4j.ReferenceParams;
import org.eclipse.lsp4j.RenameParams;
import org.eclipse.lsp4j.SignatureHelp;
import org.eclipse.lsp4j.SymbolInformation;
import org.eclipse.lsp4j.TextDocumentPositionParams;
import org.eclipse.lsp4j.TextEdit;
import org.eclipse.lsp4j.WorkspaceEdit;
import org.eclipse.lsp4j.jsonrpc.CompletableFutures;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.lsp4j.services.TextDocumentService;
import org.pshdl.model.HDLPackage;
import org.pshdl.model.validation.Problem;

import com.google.common.collect.Maps;

public class PSHDLTextDocumentService implements TextDocumentService {

    private final LanguageServerProvider lsp;
    private final Map<String, TextBuffer> tb = Maps.newConcurrentMap();
    private final ScheduledThreadPoolExecutor scheduler = createScheduler();

    public ScheduledThreadPoolExecutor createScheduler() {
        final ScheduledThreadPoolExecutor s = new ScheduledThreadPoolExecutor(1);
        s.setRemoveOnCancelPolicy(true);
        return s;
    }

    private final Map<String, ScheduledFuture<?>> tasks = Maps.newConcurrentMap();

    public PSHDLTextDocumentService(LanguageServerProvider languageServerProvider) {
        this.lsp = languageServerProvider;
    }

    @Override
    public CompletableFuture<Either<List<CompletionItem>, CompletionList>> completion(TextDocumentPositionParams position) {
        System.err.println("PSHDLTextDocumentService.completion()" + position);
        return CompletableFutures.computeAsync((cc) -> {
            final CompletionItem hover = new CompletionItem();
            hover.setKind(CompletionItemKind.Field);
            hover.setLabel("TestItem");
            return Either.forLeft(Arrays.asList(hover));
        });
    }

    @Override
    public CompletableFuture<CompletionItem> resolveCompletionItem(CompletionItem unresolved) {
        System.err.println("PSHDLTextDocumentService.resolveCompletionItem()" + unresolved);
        return null;
    }

    @Override
    public CompletableFuture<Hover> hover(TextDocumentPositionParams position) {
        System.err.println("PSHDLTextDocumentService.hover()" + position);
        return CompletableFutures.computeAsync((cc) -> {
            final Hover hover = new Hover();
            hover.setContents(Arrays.asList(Either.forLeft("Test")));
            return hover;
        });
    }

    @Override
    public CompletableFuture<SignatureHelp> signatureHelp(TextDocumentPositionParams position) {
        System.err.println("PSHDLTextDocumentService.signatureHelp()" + position);
        return null;
    }

    @Override
    public CompletableFuture<List<? extends Location>> definition(TextDocumentPositionParams position) {
        System.err.println("PSHDLTextDocumentService.definition()" + position);
        return null;
    }

    @Override
    public CompletableFuture<List<? extends Location>> references(ReferenceParams params) {
        System.err.println("PSHDLTextDocumentService.references()" + params);
        return null;
    }

    @Override
    public CompletableFuture<List<? extends DocumentHighlight>> documentHighlight(TextDocumentPositionParams position) {
        System.err.println("PSHDLTextDocumentService.documentHighlight()" + position);
        return null;
    }

    @Override
    public CompletableFuture<List<? extends SymbolInformation>> documentSymbol(DocumentSymbolParams params) {
        System.err.println("PSHDLTextDocumentService.documentSymbol()" + params);
        return null;
    }

    @Override
    public CompletableFuture<List<? extends Command>> codeAction(CodeActionParams params) {
        System.err.println("PSHDLTextDocumentService.codeAction()" + params);
        return null;
    }

    @Override
    public CompletableFuture<List<? extends CodeLens>> codeLens(CodeLensParams params) {
        System.err.println("PSHDLTextDocumentService.codeLens()" + params);
        return null;
    }

    @Override
    public CompletableFuture<CodeLens> resolveCodeLens(CodeLens unresolved) {
        System.err.println("PSHDLTextDocumentService.resolveCodeLens()" + unresolved);
        return null;
    }

    @Override
    public CompletableFuture<List<? extends TextEdit>> formatting(DocumentFormattingParams params) {
        System.err.println("PSHDLTextDocumentService.formatting()" + params);
        return null;
    }

    @Override
    public CompletableFuture<List<? extends TextEdit>> rangeFormatting(DocumentRangeFormattingParams params) {
        System.err.println("PSHDLTextDocumentService.rangeFormatting()" + params);
        return null;
    }

    @Override
    public CompletableFuture<List<? extends TextEdit>> onTypeFormatting(DocumentOnTypeFormattingParams params) {
        System.err.println("PSHDLTextDocumentService.onTypeFormatting()" + params);
        return null;
    }

    @Override
    public CompletableFuture<WorkspaceEdit> rename(RenameParams params) {
        System.err.println("PSHDLTextDocumentService.rename()" + params);
        return null;
    }

    @Override
    public void didOpen(DidOpenTextDocumentParams params) {
        final String content = params.getTextDocument().getText();
        final String docUri = params.getTextDocument().getUri();
        final Future<HDLPackage> reparse = reparse(content, docUri);
        try {
            addFile(content, docUri, reparse.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

    }

    public void addFile(final String content, final String docUri, HDLPackage pkg) {
        final TextBuffer tb = new TextBuffer();
        tb.setContent(content, pkg);
        this.tb.put(docUri, tb);
    }

    @Override
    public void didChange(DidChangeTextDocumentParams params) {
        final String content = params.getContentChanges().get(0).getText();
        final String docUri = params.getTextDocument().getUri();
        scheduleReparse(content, docUri);

    }

    public void scheduleReparse(final String content, final String docUri) {
        final ScheduledFuture<?> future = tasks.get(docUri);
        if (future != null) {
            future.cancel(true);
        }
        final ScheduledFuture<?> scheduledFuture = scheduler.schedule(() -> {
            final Future<HDLPackage> reparse = reparse(content, docUri);
            try {
                this.tb.get(docUri).setContent(content, reparse.get());
            } catch (final InterruptedException e) {
                e.printStackTrace();
            } catch (final ExecutionException e) {
                e.printStackTrace();
            }
        }, 100, TimeUnit.MILLISECONDS);
        tasks.put(docUri, scheduledFuture);
    }

    public Future<HDLPackage> reparse(String content, final String docUri) {
        final String path = URI.create(docUri).getPath();
        final File file = new File(path);
        return LanguageServerProvider.executor.submit(() -> {
            final boolean add = lsp.compiler.add(content, path, null);
            final Set<Problem> problems = lsp.compiler.getProblems(file);
            HDLPackage pkg = null;
            if (!add) {
                pkg = lsp.compiler.getFileUnits().get(lsp.compiler.getSourceName(file));
                try {
                    lsp.compiler.validateFile(pkg, problems);
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            }
            lsp.reportProblem(docUri, tb.get(docUri), problems);
            return pkg;
        });
    }

    @Override
    public void didClose(DidCloseTextDocumentParams params) {
        System.err.println("PSHDLTextDocumentService.didClose()");

    }

    @Override
    public void didSave(DidSaveTextDocumentParams params) {
        final String content = params.getText();
        final String docUri = params.getTextDocument().getUri();
        if (content != null) {
            scheduleReparse(content, docUri);
        }
    }

    public TextBuffer getBuffer(String docURI) {
        return tb.get(docURI);
    }

}
