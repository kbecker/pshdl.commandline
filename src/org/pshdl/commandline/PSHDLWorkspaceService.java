package org.pshdl.commandline;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.eclipse.lsp4j.DidChangeConfigurationParams;
import org.eclipse.lsp4j.DidChangeWatchedFilesParams;
import org.eclipse.lsp4j.Location;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.SymbolInformation;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.lsp4j.WorkspaceSymbolParams;
import org.eclipse.lsp4j.jsonrpc.CompletableFutures;
import org.eclipse.lsp4j.services.WorkspaceService;
import org.pshdl.model.parser.SourceInfo;
import org.pshdl.model.utils.HDLQualifiedName;

public class PSHDLWorkspaceService implements WorkspaceService {

    private final LanguageServerProvider lsp;

    public PSHDLWorkspaceService(LanguageServerProvider languageServerProvider) {
        this.lsp = languageServerProvider;
    }

    @Override
    public CompletableFuture<List<? extends SymbolInformation>> symbol(WorkspaceSymbolParams params) {
        final String query = params.getQuery();
        System.err.println("PSHDLWorkspaceService.symbol()" + params);
        return CompletableFutures.computeAsync(
                (cc) -> lsp.library.variables.keySet().stream().filter((k) -> k.toString().matches(query)).map(this::toSymbolInformation).collect(Collectors.toList()));
    }

    public SymbolInformation toSymbolInformation(HDLQualifiedName varName) {
        final SourceInfo src = lsp.library.variables.get(varName).getMeta(SourceInfo.INFO);
        final Range range = new Range(new Position(src.startLine, src.startPosInLine), new Position(src.endLine, src.endPosInLine));
        final Location location = new Location(lsp.library.getSrc(varName), range);
        return new SymbolInformation(varName.toString(), SymbolKind.Field, location);
    }

    @Override
    public void didChangeConfiguration(DidChangeConfigurationParams params) {
        System.err.println("PSHDLWorkspaceService.didChangeConfiguration()" + params);
    }

    @Override
    public void didChangeWatchedFiles(DidChangeWatchedFilesParams params) {
        System.err.println("PSHDLWorkspaceService.didChangeWatchedFiles()" + params);
    }

}
