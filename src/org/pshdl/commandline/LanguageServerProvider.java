package org.pshdl.commandline;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.eclipse.lsp4j.CompletionOptions;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.lsp4j.ExecuteCommandOptions;
import org.eclipse.lsp4j.InitializeParams;
import org.eclipse.lsp4j.InitializeResult;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.PublishDiagnosticsParams;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.SaveOptions;
import org.eclipse.lsp4j.ServerCapabilities;
import org.eclipse.lsp4j.TextDocumentSyncKind;
import org.eclipse.lsp4j.TextDocumentSyncOptions;
import org.eclipse.lsp4j.jsonrpc.CompletableFutures;
import org.eclipse.lsp4j.jsonrpc.Launcher;
import org.eclipse.lsp4j.launch.LSPLauncher;
import org.eclipse.lsp4j.services.LanguageClient;
import org.eclipse.lsp4j.services.LanguageServer;
import org.eclipse.lsp4j.services.TextDocumentService;
import org.eclipse.lsp4j.services.WorkspaceService;
import org.pshdl.generator.vhdl.PStoVHDLCompiler;
import org.pshdl.model.HDLPackage;
import org.pshdl.model.utils.HDLCore;
import org.pshdl.model.utils.HDLLibrary;
import org.pshdl.model.utils.services.IOutputProvider;
import org.pshdl.model.validation.HDLValidator.HDLAdvise;
import org.pshdl.model.validation.Problem;
import org.pshdl.model.validation.builtin.BuiltInAdvisor;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import com.google.common.io.Resources;

public class LanguageServerProvider implements IOutputProvider, LanguageServer {

    public LanguageServerProvider() {
        textDocumentService = new PSHDLTextDocumentService(this);
        workspaceService = new PSHDLWorkspaceService(this);
    }

    private final class PSHDLFiles implements FileFilter {
        @Override
        public boolean accept(File pathname) {
            final String name = pathname.getName();
            // if (pathname.isDirectory()) {
            // System.err.println("LanguageServerProvider.PSHDLFiles.accept() Dive into:" + name);
            // pathname.listFiles(this);
            // }
            if (name.endsWith(".pshdl")) {
                System.err.println("LanguageServerProvider.PSHDLFiles.accept()" + name);
                try {
                    final String content = Files.toString(pathname, Charsets.UTF_8);
                    final String docURI = pathname.toURI().toString();
                    final String src = compiler.getSourceName(pathname);
                    HDLPackage hdlPackage = null;
                    if (compiler.add(content, src, null)) {
                        hdlPackage = compiler.getFileUnits().get(src);
                    }
                    textDocumentService.addFile(content, docURI, hdlPackage);
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            }
            if (name.endsWith(".vhdl") || name.endsWith(".vhd")) {
                System.err.println("LanguageServerProvider.PSHDLFiles.accept()" + name);
                try {
                    PStoVHDLCompiler.addVHDL(compiler, pathname);
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
            return false;
        }
    }

    public static ExecutorService executor = Executors.newCachedThreadPool();

    public LanguageClient languageClient;
    public HDLLibrary library = new HDLLibrary();
    public File workspace;
    public PStoVHDLCompiler compiler;

    private final PSHDLTextDocumentService textDocumentService;

    private final PSHDLWorkspaceService workspaceService;

    @Override
    public String getHookName() {
        return "lsp";
    }

    @Override
    public MultiOption getUsage() {
        return new MultiOption(null, null, new Options());
    }

    @Override
    public String invoke(CommandLine cli) throws Exception {
        start();
        return null;
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException, MalformedURLException, IOException {
        HDLCore.defaultInit();
        new LanguageServerProvider().start();
        // test();
    }

    public static void test() throws InterruptedException, IOException, MalformedURLException {
        final LanguageServerProvider lsp = new LanguageServerProvider();
        final Launcher<LanguageClient> launcher = LSPLauncher.createServerLauncher(lsp, System.in, System.out, false, new PrintWriter(System.err));
        lsp.languageClient = launcher.getRemoteProxy();
        final Future<?> future = launcher.startListening();
        lsp.workspace = new File("/Users/karstenbecker/ownCloud/Shared/PTS_Electronics/FPGA/VFE_Code/");
        lsp.compiler = new PStoVHDLCompiler(lsp.workspace.toURI().toString(), executor);
        lsp.initialScanOfWorkspace();
        Thread.sleep(5000);
        final File file = new File(lsp.workspace, "AXI_Test.pshdl");
        lsp.textDocumentService.reparse(Resources.toString(file.toURL(), Charsets.UTF_8), file.toURI().toASCIIString());
    }

    public void start() throws InterruptedException, ExecutionException {
        final Launcher<LanguageClient> launcher = LSPLauncher.createServerLauncher(this, System.in, System.out, false, new PrintWriter(System.err));
        languageClient = launcher.getRemoteProxy();
        launcher.startListening().get();
    }

    @Override
    public CompletableFuture<InitializeResult> initialize(InitializeParams params) {
        if (compiler != null) {
            compiler.close();
        }
        if (params.getRootPath() != null) {
            workspace = new File(params.getRootPath());
        } else {
            final URI uri = URI.create(params.getRootUri());
            workspace = new File(uri.getPath());
        }
        final String workspaceURI = workspace.toURI().toString();
        compiler = new PStoVHDLCompiler(workspaceURI, executor);
        library = HDLLibrary.getLibrary(workspaceURI);
        executor.submit(() -> {
            System.err.println("LanguageServerProvider.initialize() Submitting scanner");
            initialScanOfWorkspace();
        });
        return CompletableFutures.computeAsync((a) -> new InitializeResult(getServerCapabilities()));
    }

    public void initialScanOfWorkspace() {
        workspace.listFiles(new PSHDLFiles());
        try {
            final boolean validatePackages = compiler.validatePackages();
            System.err.println("LanguageServerProvider.initialize() validated packages:" + validatePackages);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        reportProblems();
    }

    public void reportProblems() {
        final Map<String, Set<Problem>> problems = compiler.getAllProblems();
        for (final Entry<String, Set<Problem>> entry : problems.entrySet()) {
            final String docURI = new File(entry.getKey()).toURI().toString();
            reportProblem(entry.getKey(), textDocumentService.getBuffer(docURI), entry.getValue());
        }
    }

    public void reportProblem(String src, TextBuffer textBuffer, Set<Problem> problems) {
        try {
            final PublishDiagnosticsParams diags = new PublishDiagnosticsParams();
            diags.setUri(src);
            final List<Diagnostic> diagnostics = Lists.newArrayList();
            for (final Problem problem : problems) {
                diagnostics.add(problemToDiagnostic(src, textBuffer, problem));
            }
            diags.setDiagnostics(diagnostics);
            languageClient.publishDiagnostics(diags);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    private Diagnostic problemToDiagnostic(String src, TextBuffer textBuffer, Problem problem) {
        final Diagnostic diag = new Diagnostic();
        diag.setCode(problem.code.name());
        final HDLAdvise advise = BuiltInAdvisor.advise(problem);
        if (advise != null) {
            diag.setMessage(advise.message);
        } else {
            diag.setMessage(problem.code + ":" + problem.info);
        }
        diag.setSource(src);
        switch (problem.severity) {
        case ERROR:
            diag.setSeverity(DiagnosticSeverity.Error);
            break;
        case WARNING:
            diag.setSeverity(DiagnosticSeverity.Warning);
            break;
        case INFO:
            diag.setSeverity(DiagnosticSeverity.Information);
            break;
        }
        final int startLine = problem.line - 1;
        int endLine = startLine;
        int endLinePosition = problem.offsetInLine + problem.length;
        if (textBuffer != null) {
            final int startOffset = textBuffer.lineToOffset(startLine) + problem.offsetInLine;
            final int endOffset = startOffset + problem.length;
            final int endLineOffset = textBuffer.lineToOffset(endLine);
            endLine = textBuffer.offsetToLine(endOffset);
            endLinePosition = endOffset - endLineOffset;
        }
        diag.setRange(new Range(new Position(startLine, problem.offsetInLine), new Position(endLine, endLinePosition)));
        return diag;
    }

    public ServerCapabilities getServerCapabilities() {
        final ServerCapabilities serverCapabilities = new ServerCapabilities();
        final TextDocumentSyncOptions textSync = new TextDocumentSyncOptions();
        textSync.setChange(TextDocumentSyncKind.Full);
        textSync.setSave(new SaveOptions(true));
        // serverCapabilities.setHoverProvider(true);
        // serverCapabilities.setCodeActionProvider(true);
        // serverCapabilities.setCodeLensProvider(new CodeLensOptions(false));
        serverCapabilities.setExecuteCommandProvider(new ExecuteCommandOptions(Arrays.asList("compile", "validate")));
        serverCapabilities.setCompletionProvider(new CompletionOptions(false, Arrays.asList(".", "=")));
        serverCapabilities.setWorkspaceSymbolProvider(true);
        serverCapabilities.setTextDocumentSync(textSync);
        return serverCapabilities;
    }

    @Override
    public CompletableFuture<Object> shutdown() {
        System.err.println("shutdown()");
        if (compiler != null) {
            compiler.close();
        }
        return CompletableFuture.completedFuture(null);
    }

    @Override
    public void exit() {
        System.err.println("exit()");
    }

    @Override
    public TextDocumentService getTextDocumentService() {
        return textDocumentService;
    }

    @Override
    public WorkspaceService getWorkspaceService() {
        return workspaceService;
    }

}
