package org.pshdl.commandline;

import java.util.List;

import org.pshdl.model.HDLPackage;

import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;

public class TextBuffer {

    private String content;
    private int[] lines;
    private HDLPackage pkg;

    public String getContent() {
        return content;
    }

    public void setContent(String content, HDLPackage pkg) {
        this.pkg = pkg;
        this.content = content;
        final List<Integer> lines = Lists.newArrayList();
        lines.add(0);
        for (int i = 0; i < (content.length() - 1); i++) {
            if (content.charAt(i) == '\r') {
                if (content.charAt(i + 1) == '\n') {
                    i++;
                }
                lines.add(i + 1);
            }
            if (content.charAt(i) == '\n') {
                lines.add(i + 1);
            }
        }
        this.lines = Ints.toArray(lines);
    }

    public int lineToOffset(int line) {
        if ((line >= lines.length) || (line < 0)) {
            return -1;
        }
        return lines[line];
    }

    public int offsetToLine(int offset) {
        return binary_search(lines, offset, 0, lines.length - 1);
    }

    private int binary_search(int A[], int key, int imin, int imax) {
        // test if array is empty
        if (imax < imin) {
            return -1;
        }
        // calculate midpoint to cut set in half
        final int imid = (imin + imax) / 2;
        if (A[imid] > key) {
            return binary_search(A, key, imin, imid - 1);
        } else if (A[imid] < key) {
            if (((imid + 1) <= (A.length - 1)) && (A[imid + 1] > key)) {
                return imid;
            }
            return binary_search(A, key, imid + 1, imax);
        } else {
            return imid;
        }
    }

    public String getLine(int startLine) {
        return content.substring(lines[startLine], lines[startLine + 1] - 1);
    }
}
